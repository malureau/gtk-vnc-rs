// Take a look at the license at the top of the repository in the LICENSE file.

//! # GVnc bindings
//!
//! This library contains safe Rust bindings for GVnc.

#![cfg_attr(docsrs, feature(doc_cfg))]

pub use ffi;
pub use gio;
pub use glib;

// no runtime to initialize
macro_rules! assert_initialized_main_thread {
    () => {};
}

// No-op
macro_rules! skip_assert_initialized {
    () => {};
}

#[allow(unused_imports)]
mod auto;
pub use auto::*;

pub mod prelude;
pub mod subclass;

mod connection;
mod cursor;
mod framebuffer;
pub use framebuffer::*;
mod pixel_format;
pub use pixel_format::*;

pub mod builders {
     pub use super::auto::builders::*;
}

#[doc(alias = "vnc_util_set_debug")]
pub fn set_debug(enabled: bool) {
    unsafe { ffi::vnc_util_set_debug(enabled as i32) }
}
