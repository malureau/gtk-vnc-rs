use std::ffi::CString;

use glib::translate::*;

use crate::Connection;

impl Connection {
    #[doc(alias = "vnc_connection_client_cut_text")]
    pub fn client_cut_text(&self, text: &str) -> bool {
        let text = CString::new(text).unwrap();
        unsafe {
            from_glib(ffi::vnc_connection_client_cut_text(
                self.to_glib_none().0,
                text.as_ptr() as _,
                text.as_bytes_with_nul().len(),
            ))
        }
    }
}
