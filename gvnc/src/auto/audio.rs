// This file was generated by gir (https://github.com/gtk-rs/gir)
// from
// from gir-files (https://github.com/gtk-rs/gir-files.git)
// DO NOT EDIT

use crate::{ffi, AudioFormat, AudioSample};
use glib::{prelude::*, translate::*};

glib::wrapper! {
    #[doc(alias = "VncAudio")]
    pub struct Audio(Interface<ffi::VncAudio, ffi::VncAudioInterface>);

    match fn {
        type_ => || ffi::vnc_audio_get_type(),
    }
}

impl Audio {
    pub const NONE: Option<&'static Audio> = None;
}

pub trait AudioExt: IsA<Audio> + 'static {
    #[doc(alias = "vnc_audio_playback_data")]
    fn playback_data(&self, sample: &mut AudioSample) {
        unsafe {
            ffi::vnc_audio_playback_data(
                self.as_ref().to_glib_none().0,
                sample.to_glib_none_mut().0,
            );
        }
    }

    #[doc(alias = "vnc_audio_playback_start")]
    fn playback_start(&self, format: &mut AudioFormat) {
        unsafe {
            ffi::vnc_audio_playback_start(
                self.as_ref().to_glib_none().0,
                format.to_glib_none_mut().0,
            );
        }
    }

    #[doc(alias = "vnc_audio_playback_stop")]
    fn playback_stop(&self) {
        unsafe {
            ffi::vnc_audio_playback_stop(self.as_ref().to_glib_none().0);
        }
    }
}

impl<O: IsA<Audio>> AudioExt for O {}
