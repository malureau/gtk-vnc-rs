use glib::{subclass::prelude::*, Object};

use crate::BaseFramebuffer;

pub trait BaseFramebufferImpl: ObjectImpl {}

unsafe impl<T: BaseFramebufferImpl> IsSubclassable<T> for BaseFramebuffer {
    fn class_init(class: &mut glib::Class<Self>) {
        <Object as IsSubclassable<T>>::class_init(class);
    }

    fn instance_init(instance: &mut glib::subclass::InitializingObject<T>) {
        <Object as IsSubclassable<T>>::instance_init(instance);
    }
}
