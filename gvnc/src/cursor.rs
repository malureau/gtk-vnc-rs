use std::slice;

use glib::translate::*;

use crate::Cursor;

impl Cursor {
    #[doc(alias = "vnc_cursor_get_data")]
    pub fn data(&self) -> &[u8] {
        let len = self.width() * self.height() * 4;
        unsafe {
            slice::from_raw_parts(ffi::vnc_cursor_get_data(self.to_glib_none().0), len.into())
        }
    }
}
