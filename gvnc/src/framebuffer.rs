use std::slice;

use glib::object::IsA;
use glib::translate::*;

use crate::{auto::traits::FramebufferExt, Framebuffer};

pub trait FramebufferManualExt: 'static {
    #[doc(alias = "vnc_framebuffer_get_buffer")]
    fn buffer(&self) -> &[u8];
}

impl<O: IsA<Framebuffer>> FramebufferManualExt for O {
    fn buffer(&self) -> &[u8] {
        let len = self.height() as usize * self.rowstride() as usize;
        unsafe {
            slice::from_raw_parts(
                ffi::vnc_framebuffer_get_buffer(self.as_ref().to_glib_none().0),
                len,
            )
        }
    }
}
