use glib::translate::*;
use std::convert::TryInto;

use crate::PixelFormat;

pub enum ByteOrder {
    Little,
    Big,
    Unknown(u16),
}

impl PixelFormat {
    pub fn new_with(
        max: (u16, u16, u16),
        shift: (u8, u8, u8),
        depth: u8,
        bits_per_pixel: u8,
        byte_order: ByteOrder,
        true_color_flag: u8,
    ) -> Result<Self, String> {
        let mut pf = Self::new();

        let self_ = pf.to_glib_none_mut().0;
        unsafe {
            (*self_).red_max = max.0;
            (*self_).green_max = max.1;
            (*self_).blue_max = max.2;
            (*self_).red_shift = shift.0;
            (*self_).green_shift = shift.1;
            (*self_).blue_shift = shift.2;
            (*self_).depth = depth;
            (*self_).bits_per_pixel = bits_per_pixel;
            (*self_).true_color_flag = true_color_flag;
        }

        pf.set_byte_order(byte_order);
        Ok(pf)
    }

    pub fn set_byte_order(&mut self, order: ByteOrder) {
        let self_: *mut ffi::VncPixelFormat = self.to_glib_none().0;
        unsafe {
            (*self_).byte_order = match order {
                ByteOrder::Little => glib::ffi::G_LITTLE_ENDIAN.try_into().unwrap(),
                ByteOrder::Big => glib::ffi::G_BIG_ENDIAN.try_into().unwrap(),
                ByteOrder::Unknown(other) => other,
            }
        }
    }

    pub fn byte_order(&self) -> ByteOrder {
        let self_: *const ffi::VncPixelFormat = self.to_glib_none().0;
        unsafe {
            match (*self_).byte_order {
                b if b as i32 == glib::ffi::G_LITTLE_ENDIAN => ByteOrder::Little,
                b if b as i32 == glib::ffi::G_BIG_ENDIAN => ByteOrder::Big,
                other => ByteOrder::Unknown(other),
            }
        }
    }
}
